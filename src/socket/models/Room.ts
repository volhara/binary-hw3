import { User } from "./User";

export class Room {
  public roomName: string;
  public users: Array<User>;
  public finishRate: Map<string, number>;
  public isGameInProcess: boolean;

  constructor(roomName: string, users: Array<User>) {
    this.roomName = roomName;
    this.users = users;
    this.finishRate = new Map<string, number>();
    this.isGameInProcess = false;
  }
  public AddUser(user: User) {
    this.users.push(user);
    user.lastRoom = this.roomName;
    user.readyState = false;
  }

  public DeleteUser(user: User) {
    this.finishRate.delete(user.userName);
    this.users.splice(this.users.indexOf(user), 1);
  }
  public GetAllNamesOfUsers(): Array<string> {
    return this.users.map((user) => user.userName);
  }
  public GetInfo(): any {
    return {
      roomName: this.roomName,
      users: this.users.map((user) => user.userName),
      numberOfUsers: this.users.length,
    };
  }
  public GameStart() {
    this.isGameInProcess = true;
    this.finishRate = new Map<string, number>();
  }
  public GameEnd() {
    this.isGameInProcess = false;
    this.finishRate = new Map<string, number>();
  }
  public SetWin(userName: string, userScore: number) {
    this.finishRate.set(userName, userScore);
  }
}
