export class User {
  public userName: string;
  public readyState: boolean;
  public userId: string;
  public lastRoom: string | null;
  public proggress: number = 0;

  constructor(userName: string, userId: string) {
    this.userName = userName;
    this.userId = userId;
    this.readyState = false;
    this.lastRoom = null;
  }

  public GetInfo(currentUserId: string): any {
    return {
      userName: this.userName,
      readyState: this.readyState,
      isLocalUser: this.userId === currentUserId,
    };
  }
}
