import { Server } from "socket.io";
import game from "./game";

export default (io: Server) => {
  game(io.of("/lobby"));
};
