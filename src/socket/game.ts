import { Namespace, Server, Socket } from "socket.io";
import { texts } from "../data";
import {
  MAXIMUM_USERS_FOR_ONE_ROOM,
  SECONDS_FOR_GAME,
  SECONDS_TIMER_BEFORE_START_GAME,
} from "./config";
import { Room } from "./models/Room";
import { User } from "./models/User";

const users = new Array<User>();
const rooms = new Array<Room>();

const getUserByName = (userName: string): User => {
  return users.find((x) => x.userName == userName) as User;
};

const getRoomByName = (roomName): Room => {
  return rooms.find((x) => x.roomName == roomName) as Room;
};

const getRandomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

const deleteEmptyRooms = (io: Namespace<any>) => {
  for (let index = 0; index < rooms.length; index++) {
    if (rooms[index].users.length == 0) {
      io.emit("REMOVE_ROOM", rooms[index].GetInfo());
      rooms.splice(index, 1);
    }
  }
};

const checkWin = (room: Room): boolean => {
  let isWin: boolean = true;
  for (let index = 0; index < room.users.length; index++) {
    if (room.users[index].proggress < 100) {
      isWin = false;
    }
  }
  return isWin;
};

const checkAllReadyStatus = (room: Room): boolean => {
  if (room.users.length == 0) {
    return false;
  }
  let result: boolean = true;
  for (const user of room.users) {
    if (!user.readyState) {
      result = false;
    }
  }
  return result;
};

export default (io: Namespace<any>) => {
  io.on("connection", (socket: Socket<any>) => {
    const username = socket.handshake.query.username as string;
    let currentUser: User;
    if (getUserByName(username) || username == null) {
      socket.emit("BAD_LOGIN", {
        message: "User with this name is already login",
      });
      socket.disconnect();
    } else {
      currentUser = new User(username, socket.id);
      users.push(currentUser);
    }

    socket.emit(
      "ROOM_LIST",
      rooms.map((room) => {
        if (
          room.users.length > 0 &&
          room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM &&
          !room.isGameInProcess
        ) {
          return room.GetInfo();
        }
      })
    );

    socket.on("CHANGE_USER_PROGRESS", ({ proggress, timeInFinish }) => {
      currentUser.proggress = proggress;
      io.to(currentUser.lastRoom as string).emit("UPDATE_USER_PROGRESS", {
        userName: currentUser.userName,
        progress: proggress,
      });
      let room = getRoomByName(currentUser.lastRoom);
      if (proggress >= 100) {
        room.SetWin(currentUser.userName, timeInFinish);
      }
      if (checkWin(room)) {
        let winList: Array<any> = new Array<any>();
        for (const rate of room.finishRate) {
          winList.push({ userName: rate[0] });
        }
        io.to(currentUser.lastRoom as string).emit("GAME_END", winList);
      }
    });

    socket.on("JOIN_TO_ROOM", (roomName: string) => {
      let room = getRoomByName(roomName);
      room.AddUser(currentUser);
      currentUser.lastRoom = roomName;
      socket.emit("CONNECT_TO_ROOM", {
        roomName: roomName,
        users: room.users.map((user) => user.GetInfo(currentUser.userId)),
      });
      io.to(roomName).emit("NEW_USER_IN_ROOM", currentUser.GetInfo("1"));
      socket.join(roomName);
      if (room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM) {
        io.emit("ROOM_UPDATE_USER_COUNT", room.GetInfo());
      } else {
        io.emit("REMOVE_ROOM", room.GetInfo());
      }
    });

    socket.on("CHANGE_READY_STATE", () => {
      currentUser.readyState = !currentUser.readyState;
      let room = getRoomByName(currentUser.lastRoom);
      io.to(currentUser.lastRoom as string).emit(
        "USER_READY_STATE_CHANGED",
        currentUser.GetInfo("1")
      );
      if (checkAllReadyStatus(room)) {
        let id = getRandomNumber(0, texts.length);
        room.GameStart();
        io.emit("REMOVE_ROOM", room.GetInfo());
        io.to(currentUser.lastRoom as string).emit("START_TIMER", {
          id: id,
          secondBeforeStart: SECONDS_TIMER_BEFORE_START_GAME,
          secondForGame: SECONDS_FOR_GAME,
        });
      }
    });

    socket.on("CREATE_ROOM", async (roomName: string) => {
      if (getRoomByName(roomName) != null) {
        socket.emit("BAD_ROOM", {
          message: "Room with this name is already exist",
        });
      } else {
        let createdRoom = new Room(roomName, [currentUser]);
        rooms.push(createdRoom);
        currentUser.lastRoom = roomName;
        socket.emit("CONNECT_TO_ROOM", {
          roomName: roomName,
          users: createdRoom.users.map((user) =>
            user.GetInfo(currentUser.userId)
          ),
        });
        await socket.join(roomName);

        io.emit("ROOM_APPEND", createdRoom.GetInfo());
      }
    });

    socket.on("USER_EXIT_FROM_ROOM", () => {
      io.to(currentUser.lastRoom as string).emit("DELETE_USER_FROM_ROOM", {
        userName: currentUser.userName,
      });
      socket.leave(currentUser.lastRoom as string);
      let room = getRoomByName(currentUser.lastRoom);
      room.DeleteUser(currentUser);
      if (checkAllReadyStatus(room)) {
        let id = getRandomNumber(0, texts.length);
        io.to(room.roomName).emit("START_TIMER", {
          id: id,
          secondBeforeStart: SECONDS_TIMER_BEFORE_START_GAME,
        });
      }
      if (room.users.length == MAXIMUM_USERS_FOR_ONE_ROOM - 1) {
        io.emit("ROOM_APPEND", room.GetInfo());
      } else {
        io.emit("ROOM_UPDATE_USER_COUNT", room.GetInfo());
      }
      currentUser.lastRoom = null;
      deleteEmptyRooms(io);
    });

    socket.on("disconnect", () => {
      let user = getUserByName(username);
      if (user.userId == socket.id) {
        if (user.lastRoom != null) {
          io.to(currentUser.lastRoom as string).emit("DELETE_USER_FROM_ROOM", {
            userName: currentUser.userName,
          });
          let room = getRoomByName(user.lastRoom);
          room.DeleteUser(user);

          if (checkAllReadyStatus(room)) {
            let id = getRandomNumber(0, texts.length);
            io.to(room.roomName).emit("START_TIMER", {
              id: id,
              secondBeforeStart: SECONDS_TIMER_BEFORE_START_GAME,
            });
          }

          if (room.users.length == MAXIMUM_USERS_FOR_ONE_ROOM - 1) {
            io.emit("ROOM_APPEND", room.GetInfo());
          } else {
            io.emit("ROOM_UPDATE_USER_COUNT", room.GetInfo());
          }

          deleteEmptyRooms(io);
        }
        users.splice(users.indexOf(user), 1);
      }
    });
  });
};
