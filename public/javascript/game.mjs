import { showMessageModal } from "./views/modal.mjs";
import { showInputModal } from "./views/modal.mjs";
import {
  appendRoomElement,
  removeRoomElement,
  updateNumberOfUsersInRoom,
} from "./views/room.mjs";
import { removeClass, addClass, createElement } from "./helpers/domHelper.mjs";
import {
  appendUserElement,
  removeUserElement,
  removeAllUserElement,
  changeReadyStatus,
  setProgress,
} from "./views/user.mjs";

const username = sessionStorage.getItem("username");
const createRoomBtn = document.getElementById("add-room-btn");
const gameContainer = document.getElementById("game-page");
const lobbyContainer = document.getElementById("rooms-page");
const backToLobbyBtn = document.getElementById("quit-room-btn");
const roomNameH1 = document.getElementById("room-name");
const readyBtn = document.getElementById("ready-btn");
const timerDiv = document.getElementById("timer");
const gameTimer = document.getElementById("game-timer");

var socket;

const onBadLogin = (error) => {
  showMessageModal({
    message: error.message,
    onClose: () => {
      sessionStorage.clear();
      window.location.replace("/login");
    },
  });
};

const onBadRoomCreating = (error) => {
  showMessageModal({
    message: error.message,
  });
};

const onReceiveRoomList = (roomList) => {
  console.log(roomList);
  for (const room of roomList) {
    appendRoomElement({
      name: room.roomName,
      numberOfUsers: room.numberOfUsers,
      onJoin: () => {
        socket.emit("JOIN_TO_ROOM", room.roomName);
      },
    });
  }
};

const onRoomAppend = (room) => {
  appendRoomElement({
    name: room.roomName,
    numberOfUsers: room.numberOfUsers,
    onJoin: () => {
      socket.emit("JOIN_TO_ROOM", room.roomName);
    },
  });
};

const onConnectToRoom = (roomInfo) => {
  removeAllUserElement();
  roomNameH1.innerText = roomInfo.roomName;
  console.log(roomInfo);
  removeClass(gameContainer, "display-none");
  addClass(lobbyContainer, "display-none");
  for (const user of roomInfo.users) {
    appendUserElement({
      username: user.userName,
      ready: user.ready,
      isCurrentUser: user.isLocalUser,
    });
  }
};

const onBackToLobbyClick = () => {
  removeClass(lobbyContainer, "display-none");
  addClass(gameContainer, "display-none");
  socket.emit("USER_EXIT_FROM_ROOM");
};

const onClickCreateButton = () => {
  showInputModal({
    title: "Enter room name",
    onSubmit: (roomName) => {
      socket.emit("CREATE_ROOM", roomName);
    },
  });
};

const onUserJoinInRoom = (user) => {
  console.log(user);
  appendUserElement({
    username: user.userName,
    ready: user.readyState,
    isCurrentUser: user.isLocalUser,
  });
};

const onNeedDeleteUserFromRoom = (user) => {
  removeUserElement(user.userName);
};

const onRoomRemoved = (roomInfo) => {
  removeRoomElement(roomInfo.roomName);
};

const onNeedUpdateUserCount = (roomInfo) => {
  updateNumberOfUsersInRoom({
    name: roomInfo.roomName,
    numberOfUsers: roomInfo.numberOfUsers,
  });
};

const onReadyBtnClick = () => {
  socket.emit("CHANGE_READY_STATE");
};

const onUserReadyStateChanged = (userInfo) => {
  changeReadyStatus({
    username: userInfo.userName,
    ready: userInfo.readyState,
  });
};

const onNeedStartTime = (randomText) => {
  console.log(randomText.id);
  addClass(backToLobbyBtn, "display-none");
  addClass(readyBtn, "display-none");
  removeClass(timerDiv, "display-none");
  timerDiv.innerText = randomText.secondBeforeStart;
  let intarvalId = setInterval(() => {
    let seconds = parseInt(timerDiv.innerText);
    timerDiv.innerText = --seconds;
    if (seconds === 0) {
      setTimeout(async () => {
        let random = await getRandomText(randomText.id);
        addClass(timerDiv, "display-none");
        startGame(random.text, randomText.secondForGame);
      }, 0);
      clearInterval(intarvalId);
    }
  }, 1000);
};

var mainText;
var mainTextLenght;

const startGame = (randomText, gameTime) => {
  mainText = randomText;
  mainTextLenght = randomText.length;
  removeClass(gameTimer, "display-none");
  let gameTimerSeconds = document.getElementById("game-timer-seconds");
  gameTimerSeconds.innerText = gameTime;
  let intarvalId = setInterval(() => {
    let seconds = parseInt(gameTimerSeconds.innerText);
    gameTimerSeconds.innerText = --seconds;
    if (seconds === 0) {
      setTimeout(async () => {
        endGame();
      }, 0);
      clearInterval(intarvalId);
    }
  }, 1000);
  var alreadyEntered = document.getElementById("alreadyEntered");
  var needToEnter = document.getElementById("needToEnter");
  var nextLetter = document.getElementById("nextLetter");
  needToEnter.textContent = randomText;
  alreadyEntered.textContent = "";

  document.addEventListener("keydown", onKeyDownInGame, false);
};

const endGame = () => {};

function onKeyDownInGame(event) {
  var firstLeater = mainText.charAt(0);
  var key = event.key;
  if (key === firstLeater) {
    needToEnter.textContent = mainText.slice(2);
    mainText = mainText.slice(1);
    alreadyEntered.textContent += firstLeater;
    nextLetter.textContent = mainText.charAt(0);
    socket.emit(
      "CHANGE_USER_PROGRESS",
      (100 * alreadyEntered.textContent.length) / mainTextLenght
    );
  }

  if (mainText.length == 0) {
    setTimeout(() => {
      document.removeEventListener("keydown", onKeyDownInGame, false);
      alert("Win!");
    }, 0);
  }
}

const getRandomText = async (id) => {
  let url = `http://localhost:3002/game/texts/${id}`;
  let response = await fetch(url);
  return await response.json();
};

const onNeedUpdateUserProgress = (userProgress) => {
  setProgress({
    username: userProgress.userName,
    progress: userProgress.progress,
  });
};

function onLoad() {
  if (!username) {
    window.location.replace("/login");
  }

  socket = io("http://localhost:3002/lobby", {
    query: { username },
    transports: ["websocket"],
    upgrade: false,
  });
  socket.on("BAD_LOGIN", onBadLogin);
  socket.on("ROOM_LIST", onReceiveRoomList);
  socket.on("BAD_ROOM", onBadRoomCreating);
  socket.on("CONNECT_TO_ROOM", onConnectToRoom);
  socket.on("ROOM_APPEND", onRoomAppend);
  socket.on("NEW_USER_IN_ROOM", onUserJoinInRoom);
  socket.on("DELETE_USER_FROM_ROOM", onNeedDeleteUserFromRoom);
  socket.on("REMOVE_ROOM", onRoomRemoved);
  socket.on("ROOM_UPDATE_USER_COUNT", onNeedUpdateUserCount);
  socket.on("USER_READY_STATE_CHANGED", onUserReadyStateChanged);
  socket.on("START_TIMER", onNeedStartTime);
  socket.on("UPDATE_USER_PROGRESS", onNeedUpdateUserProgress);

  createRoomBtn.addEventListener("click", onClickCreateButton);
  backToLobbyBtn.addEventListener("click", onBackToLobbyClick);
  readyBtn.addEventListener("click", onReadyBtnClick);
}

onLoad();
